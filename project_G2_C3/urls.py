from django.contrib import admin
from django.urls import path
from django.views.generic.base import View
from transporteApp import views
from rest_framework_simplejwt.views import TokenObtainPairView,TokenRefreshView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),

    # Endpoint busRoute model
    path('rutas/', views.BusRouteListAPIView.as_view()), # get lista de rutas
    path('rutas/<int:pk>/', views.BusRouteDetailAPIView.as_view()), # get  ruta con la llave primaria

    # Endpoints busSeat model
    path('sillas/', views.BusSeatListAPIView.as_view()), # get lista de sillas
    path('sillas/placa/<int:placa>/', views.BusSeatListPlacaAPIView.as_view()), # get lista de sillas filtrada por placa
    path('sillas/<int:pk>/', views.BusSeatDetailAPIView.as_view()), # get  silla con la llave primaria

    # Endpoint sales model
    path('compra/', views.SalesCreateAPIView.as_view()), # post venta
    path('compra/<int:user>/<int:pk>/', views.SalesDetailAPIView.as_view()), # get transacción de un usuario
    path('compra/lista_user/<int:user>/', views.SalesListUserAPIView.as_view()), # get lista de compras filtrada por usuarios
    path('compra/lista_route/<int:user>/<int:route>/', views.SalesListRouteAPIView.as_view()), # get lista de compras filtrada por usuarios
    path('compra/update/<int:user>/<int:pk>/', views.SalesUpdateAPIView.as_view()), # put actualizar registro con un usuario y un id
    path('compra/remove/<int:user>/<int:pk>/', views.SalesDetailAPIView.as_view()), # Delete a un registro con un usuario y un id
    
    # Endpoint place model 
    path('lugares/', views.PlaceListAPIView.as_view()), # get lista de lugares
    path('lugares/<int:pk>/', views.PlaceDetailAPIView.as_view()), #get lugar con la llave primaria
]
