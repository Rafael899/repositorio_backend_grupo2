from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend

from transporteApp.models.busSeat import BusSeat
from transporteApp.serializers.busSeatSerializer import BusSeatSerializer

class BusSeatListAPIView(generics.ListAPIView):
    serializer_class = BusSeatSerializer

    def get_queryset(self):
        return BusSeat.objects.all()

class BusSeatListPlacaAPIView(generics.ListAPIView):
    serializer_class = BusSeatSerializer

    def get_queryset(self):
        return BusSeat.objects.filter(placa_id=self.kwargs['placa'])
    
class BusSeatDetailAPIView(generics.RetrieveAPIView):
    serializer_class = BusSeatSerializer
    queryset = BusSeat.objects.all()

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)