from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend

from transporteApp.models.place import Place
from transporteApp.serializers.placeSerializer import PlaceSerializer

class PlaceListAPIView(generics.ListAPIView):
    serializer_class = PlaceSerializer

    def get_queryset(self):
        return Place.objects.all()

class PlaceDetailAPIView(generics.RetrieveAPIView):
    serializer_class = PlaceSerializer
    queryset = Place.objects.all()

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)