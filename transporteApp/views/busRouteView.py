from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend

from transporteApp.models.busRoute import BusRoute
from transporteApp.serializers.busRouteSerializer import BusRouteSerializer

class BusRouteListAPIView(generics.ListAPIView):
    serializer_class = BusRouteSerializer

    def get_queryset(self):
        return BusRoute.objects.all()
    
class BusRouteDetailAPIView(generics.RetrieveAPIView):
    serializer_class = BusRouteSerializer
    queryset = BusRoute.objects.all()

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)