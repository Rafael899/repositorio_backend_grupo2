from transporteApp.models.sales import Sales
from transporteApp.models.busRoute import BusRoute
from transporteApp.models.busSeat import BusSeat
from transporteApp.models.user import User
from transporteApp.models.place import Place
from rest_framework import serializers

class SalesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sales
        fields = ['user','bus_route','bus_seat']
    
    def to_representation(self, obj):
        user = User.objects.get(id=obj.user_id)
        bus_route = BusRoute.objects.get(id=obj.bus_route_id) 
        bus_seat = BusSeat.objects.get(id=obj.bus_seat_id)
        return {
            "user":{
                "id": user.id,
                "username": user.username,
                "name": user.name
            },
            "bus_route":{
                "id": bus_route.id,
                "fecha_salida": bus_route.fecha_salida,
                "precio": bus_route.precio,
                "placa": bus_route.placa_id,
                "lugar_destino_id": bus_route.lugar_destino_id,
                "lugar_origen_id": bus_route.lugar_origen_id
            },
            "bus_seat":{
                "id": bus_seat.id,
                "numero_puesto": bus_seat.numero_puesto,
                "placa": bus_seat.placa_id
            }
        }