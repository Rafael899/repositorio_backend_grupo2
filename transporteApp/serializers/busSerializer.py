from transporteApp.models.bus import Bus
from rest_framework import serializers

class BusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bus
        fields = ['placa']