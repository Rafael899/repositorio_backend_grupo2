from transporteApp.models.busRoute import BusRoute
from transporteApp.models.place import Place
from transporteApp.models.bus import Bus
from rest_framework import serializers

class BusRouteSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusRoute
        fields = ['fecha_salida','lugar_origen','lugar_destino','placa','precio']
    
    def to_representation(self, obj):
        print("=========> objeto:",obj)
        busRoute = BusRoute.objects.get(id=obj.id) 
        lugar_origen = Place.objects.get(id=obj.lugar_origen_id)
        lugar_destino = Place.objects.get(id=obj.lugar_destino_id)
        placa = Bus.objects.get(id=obj.placa_id) 
        return {
            "id": busRoute.id,  
            "fecha_salida":busRoute.fecha_salida,
            "precio": busRoute.precio,
            "placa_bus": {
                "id": placa.id,
                "placa": placa.placa
            }, 
            "lugar_origen":{
                "id":lugar_origen.id,
                "nombre_lugar": lugar_origen.nombre_lugar,
                "desc_lugar": lugar_origen.desc_lugar,
                "url_imagen": lugar_origen.url_imagen
            },
            "lugar_destino": {
                "id":lugar_destino.id,
                "nombre_lugar": lugar_destino.nombre_lugar,
                "desc_lugar": lugar_destino.desc_lugar,
                "url_imagen": lugar_destino.url_imagen
            }
        }