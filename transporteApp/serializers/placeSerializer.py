from transporteApp.models.place import Place
from rest_framework import serializers

class PlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Place
        fields = ['nombre_lugar','desc_lugar','url_imagen']