from transporteApp.models.busSeat import BusSeat
from rest_framework import serializers

class BusSeatSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusSeat
        fields = ['id','placa','numero_puesto']