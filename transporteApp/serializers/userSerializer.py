from transporteApp.models.user import User
from transporteApp.models.sales import Sales
from rest_framework import serializers
from .salesSerializer import SalesSerializer, serializers

class UserSerializer(serializers.ModelSerializer):
    #sales = SalesSerilizer()

    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'name', 'email']#, 'sales']

    def create(self, validated_data):
        #salesData = validated_data.pop('sales')
        userInstance = User.objects.create(**validated_data)
        #Sales.objects.create(user=userInstance, **salesData)
        return userInstance

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        #sales = Sales.objects.get(user=obj.id)
        return {
                'id': user.id,
                'username': user.username,
                'name': user.name,
                'email': user.email
                }