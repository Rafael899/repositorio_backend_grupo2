from django.db import models

"""
Modelo que representa los buses
"""

class Bus(models.Model):
    id = models.AutoField(primary_key=True)
    placa = models.CharField("placa_bus",max_length=6)