from django.db import models
from .bus import Bus

"""
Modelo que representa los asientos de los buses
"""
class BusSeat(models.Model):
    id = models.AutoField(primary_key=True)
    placa = models.ForeignKey(Bus, on_delete=models.CASCADE)
    numero_puesto =models.IntegerField("numero_puesto")

    class Meta:
        unique_together = ('placa','numero_puesto',)