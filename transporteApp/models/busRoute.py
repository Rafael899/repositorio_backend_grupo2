from django.db import models
from .place import Place
from .bus import Bus

"""
Modelo que representa las rutas disponibles
"""
class BusRoute(models.Model):
    id = models.AutoField(primary_key=True)
    fecha_salida = models.DateField('Fecha',auto_now_add=False)
    lugar_origen = models.ForeignKey(Place, related_name='origen', on_delete=models.CASCADE)
    lugar_destino = models.ForeignKey(Place, related_name='destino', on_delete=models.CASCADE)
    placa = models.ForeignKey(Bus,related_name='Placa', on_delete=models.CASCADE)
    precio = models.IntegerField()