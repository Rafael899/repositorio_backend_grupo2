from django.db import models

"""
Modelo que representa la lista de origenes y destinos
"""
class Place(models.Model):
    id = models.AutoField(primary_key=True)
    nombre_lugar = models.CharField('nombre',max_length=80)
    desc_lugar = models.TextField('descripción')
    url_imagen = models.TextField('url_imagen')
