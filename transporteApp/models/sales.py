from django.db import models
from .busRoute import BusRoute
from .busSeat import BusSeat
from .user import User

"""
Modelo que representa las ventas
"""
class Sales(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='usuario', on_delete=models.CASCADE)
    bus_route = models.ForeignKey(BusRoute, related_name='route', on_delete=models.CASCADE)
    bus_seat = models.ForeignKey(BusSeat, related_name='seat', on_delete=models.CASCADE)

    class Meta:
        unique_together = ('bus_route','bus_seat',)